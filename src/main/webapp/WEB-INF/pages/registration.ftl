<#-- @ftlvariable name="genders" type="cz.lax.spring.structure.domain.Gender[]" -->
<#-- @ftlvariable name="sources" type="cz.lax.spring.structure.domain.Source[]" -->
<#-- @ftlvariable name="registrationResponse" type="cz.lax.spring.structure.forms.RegistrationForm" -->

<#import 'form/form.ftl' as form />

<@master.main>
    <div class="outer">
        <section class="inner">
            <@form.form action="/registration/save" formId="registrationForm" class="form-holder">
                <section class="centralize">
                    <h1 class="mt20 mb20 ml10"><@spring.message 'form.title' /></h1>
                    <@form.input path="form.firstName" type="text" placeholder="placeholder.firstname" maxlength=50 required=true />
                    <@form.input path="form.lastName" type="text" placeholder="placeholder.lastname" maxlength=50 required=true />
                    <@form.radios path='form.gender' options=genders required=true />
                    <@form.datepicker path='form.dateOfBirth' containerClass="date" placeholder="placeholder.dateofbirth" required=true />
                    <@form.input path="form.email" type="email" placeholder="placeholder.email" maxlength=50 required=true />
                    <@form.input path="form.password" type="password" placeholder="placeholder.password" maxlength=50 required=true />
                    <@form.input path="form.confirmPassword" type="password" placeholder="placeholder.confirmpassword" maxlength=50 required=true />
                    <@form.select path="form.source" options=sources placeholder="placeholder.source" containerClass="select" required=true />
                    <@form.checkbox path="form.agreement" placeholder="placeholder.agreement" required=true/>
                    <@form.submit message="form.submit" />
                </section>
            </@form.form>
        </section>
    </div>

    <#if registrationResponse??>
        <#include 'dialogResponse.ftl' />
    </#if>

</@master.main>