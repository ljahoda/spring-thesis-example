<#include '../shared/common.ftl' />

<#macro select path options required=false containerClass='' placeholder=''>
    <@spring.bind path />
    <div class="form-row<#if containerClass?has_content> ${containerClass}</#if>">
        <select name="${spring.status.expression}" class="styledSelect"<#if required> required</#if>>
            <#if placeholder?has_content><option value=""><@spring.message placeholder /></option></#if>
            <#list options as option>
                <option value="${option.name()}"<#if (spring.stringStatusValue?? && spring.stringStatusValue == option.name()?string)> selected="selected"</#if>><@spring.message 'enum.${option.class.simpleName}.${option.name()}' /></option>
            </#list>
        </select>
        <@resolveErrors element='select' />
    </div>
</#macro>