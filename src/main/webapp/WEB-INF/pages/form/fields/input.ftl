<#include '../shared/common.ftl' />

<#macro input path type maxlength=255 required=false readonly=false customId='' placeholder='' inputClass='' containerClass=''>
    <@spring.bind path />
    <div class="form-row<#if containerClass?has_content> ${containerClass}</#if>">
        <input type="${type?string}" id="<#if customId == ''>${spring.status.expression}<#else>${customId}</#if>" name="${spring.status.expression}" maxlength="${maxlength}" value="${spring.stringStatusValue?html}"<#if inputClass?has_content> class="${inputClass}"</#if><#if placeholder?has_content> placeholder="<@spring.message placeholder />"</#if><#if required> required</#if>>
        <@resolveErrors />
    </div>
</#macro>