<#include '../shared/common.ftl' />

<#macro checkbox path required=false customId='' placeholder='' containerClass=''>
    <@spring.bind path />
    <div class="form-row<#if containerClass?has_content> ${containerClass}</#if>">
        <input type="checkbox" class="pretty" value="yes" data-color="green" name="${spring.status.expression}" id="${spring.status.expression}" data-label="<@spring.message placeholder />"<#if spring.status.value??> checked="checked"</#if><#if required> required</#if>/>
        <@resolveErrors />
    </div>
</#macro>