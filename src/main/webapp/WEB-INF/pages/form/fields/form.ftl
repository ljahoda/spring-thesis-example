<#macro form action formId='' class='' method='post' enctype='application/x-www-form-urlencoded' novalidate=true>
    <form action="<@spring.url action />"<#if formId?has_content> id="${formId}"</#if> method="${method}" enctype="${enctype?string}"<#if class?has_content> class="${class}"</#if><#if novalidate> novalidate</#if>>
        <#nested />
    </form>
</#macro>