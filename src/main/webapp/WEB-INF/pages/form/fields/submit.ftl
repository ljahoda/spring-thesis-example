<#include '../shared/common.ftl' />

<#macro submit message>
    <div class="form-row submit">
        <button type="submit" class="centralize"><@spring.message message /></button>
    </div>
</#macro>