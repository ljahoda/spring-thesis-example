<#include '../shared/common.ftl' />

<#macro datepicker path maxlength=255 required=false placeholder='' containerClass=''>
    <@spring.bind path />
    <div class="form-row<#if containerClass?has_content> ${containerClass}</#if>">
        <input type="text" name="${spring.status.expression}" maxlength="${maxlength}" value="${spring.status.value!""?html}" class="datepicker" placeholder="<@spring.message placeholder />"<#if required> required</#if>>
        <@resolveErrors />
    </div>
</#macro>