<#include '../shared/common.ftl' />

<#macro radios path options required=false containerClass=''>
    <@spring.bind path />
    <div class="form-row<#if containerClass?has_content> ${containerClass}</#if>">
        <#list options as option>
            <input type="radio" class="pretty" value="${option.name()}" data-color="green" name="${spring.status.expression}" id="${path + '-' + option.name()}" data-label="<@spring.message 'enum.${option.class.simpleName}.${option.name()}' />"<#if (spring.stringStatusValue?? && spring.stringStatusValue == option.name()?string)> checked="checked"</#if><#if required> required</#if> />
        </#list>
        <@resolveErrors />
    </div>
</#macro>