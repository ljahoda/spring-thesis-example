<#macro resolveErrors element='input'>
    <#if (spring.status.errorMessages?size > 0) >
        <p class="error"><span><@spring.showErrors "<br/>" /></span></p>
        <script>
            $(function() {
                addErrorMessage('${element}[name="${spring.status.expression}"]');
            });
        </script>
    </#if>
</#macro>