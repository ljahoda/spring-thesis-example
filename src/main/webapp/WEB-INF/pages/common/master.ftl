<#macro main>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <#include 'head.ftl' />
    </head>
    <body>
        <!-- Application container. -->
        <main role="main" id="main">
            <div class="top-row"></div>
            <#nested />
        </main>
        <#include 'include.ftl' />
    </body>
    </html>
</#macro>