<#-- @ftlvariable name="registrationResponse" type="cz.lax.spring.structure.forms.RegistrationForm" -->

<section id="responseDialog" style="display: none;">
    <div class="dBlock">
        <strong><@spring.message 'response.dialog.firstname' /> </strong> <span>${registrationResponse.firstName}</span><br>
        <strong><@spring.message 'response.dialog.lastname' /> </strong> <span>${registrationResponse.lastName}</span><br>
        <strong><@spring.message 'response.dialog.gender' /> </strong> <span><@spring.message 'enum.Gender.' + registrationResponse.gender.name() /></span><br>
        <strong><@spring.message 'response.dialog.dateofbirth' /> </strong> <span>${registrationResponse.dateOfBirth?date}</span><br>
        <strong><@spring.message 'response.dialog.email' /> </strong> <span>${registrationResponse.email}</span><br>
        <strong><@spring.message 'response.dialog.source' /> </strong> <span><@spring.message 'enum.Source.' + registrationResponse.source.name() /></span><br>
    </div>
</section>
<script>
    $(function() { showDialog('#responseDialog'); });
</script>