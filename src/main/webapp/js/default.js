$(function(){
    errorMessageHide();
    $('.styledSelect').selectBoxIt({ showEffect: "fadeIn", showEffectSpeed: 400, hideEffect: "fadeOut", hideEffectSpeed: 400, showFirstOption: false});
    $('.datepicker').datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd.mm.yy' });
    $('.pretty').each(function() { $(this).prettyCheckable(); });
});

/**
 * Error message hide
 */
function errorMessageHide() {
    $('p.error').click(function(){
        $(this).hide();
        var formRow = $(this).parent('.form-row');
        formRow.removeClass('error');
        formRow.find('input').focus();
    });
}

/**
 * Add error message
 *
 * @param selector
 */
function addErrorMessage(selector) {
    $(selector).parent('.form-row').addClass('error');
}

/**
 * Show dialog
 *
 * @param selector
 */
function showDialog(selector) {
    $(selector).dialog({
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        },
        show: {
            effect: "fadeIn",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });
}