package cz.lax.spring.structure;

import cz.lax.spring.structure.common.*;
import cz.lax.spring.structure.domain.*;
import cz.lax.spring.structure.forms.*;
import org.springframework.stereotype.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.*;

import javax.validation.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 11.03.14
 * Email: lukas@lukasjahoda.cz
 */
@Controller
@RequestMapping("/")
public class RegistrationController extends AbstractGuiController {

	private static final String REGISTRATION_VIEW_PATH = "registration";
	private static final String REGISTRATION_SAVE_PATH = "/registration/save";

	private static final String FORM_MODEL = "form";
	private static final String RESPONSE_MODEL = "registrationResponse";
	private static final String GENDERS_MODEL = "genders";
	private static final String SOURCES_MODEL = "sources";

	/**
	 * View model and view.
	 *
	 * @return the model and view
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView view() {
		return resolveView(new RegistrationForm());
	}

	/**
	 * Save model and view.
	 *
	 * @param registrationForm the registration form
	 * @param result the result
	 * @return the model and view
	 */
	@RequestMapping(value = REGISTRATION_SAVE_PATH, method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute(FORM_MODEL) @Valid RegistrationForm registrationForm, BindingResult result) {
		if (result.hasErrors()) {
			return resolveView(registrationForm);
		}

		ModelAndView modelAndView = resolveView(new RegistrationForm());
		modelAndView.addObject(RESPONSE_MODEL, registrationForm);
		return modelAndView;
	}

	/**
	 * Resolve view.
	 *
	 * @param registrationForm the registration form
	 * @return the model and view
	 */
	private ModelAndView resolveView(RegistrationForm registrationForm) {
		ModelAndView modelAndView = new ModelAndView(REGISTRATION_VIEW_PATH);
		modelAndView.addObject(FORM_MODEL, registrationForm);
		modelAndView.addObject(GENDERS_MODEL, Gender.values());
		modelAndView.addObject(SOURCES_MODEL, Source.values());
		return modelAndView;
	}

}
