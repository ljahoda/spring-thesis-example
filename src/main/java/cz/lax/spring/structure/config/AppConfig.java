package cz.lax.spring.structure.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.*;
import org.springframework.core.convert.*;
import org.springframework.core.convert.converter.*;
import org.springframework.web.servlet.i18n.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 10.03.14
 * Email: lukas@lukasjahoda.cz
 */
@Configuration
public class AppConfig {
	public static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";

	private static final String DEFAULT_LANGUAGE = "cs";

	// messages
	private static final String MESSAGES_PATH = "i18n/messages";
	private static final String MESSAGES_ENCODING = "UTF-8";

	/**
	 * Locale resolver.
	 *
	 * @return the session locale resolver
	 */
	@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(new Locale(DEFAULT_LANGUAGE));
		return localeResolver;
	}

	/**
	 * Message source.
	 *
	 * @return the resource bundle message source
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename(MESSAGES_PATH);
		source.setDefaultEncoding(MESSAGES_ENCODING);
		source.setUseCodeAsDefaultMessage(true);
		return source;
	}

	/**
	 * Gets conversion service.
	 *
	 * @return the conversion service
	 */
	@Bean(name="conversionService")
	public ConversionService getConversionService() {
		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
		bean.setConverters(getConverters());
		bean.afterPropertiesSet();
		return bean.getObject();
	}

	/**
	 * Gets converters.
	 *
	 * @return the converters
	 */
	private Set<Converter> getConverters() {
		// custom convertors
		return new HashSet<>();
	}
}
