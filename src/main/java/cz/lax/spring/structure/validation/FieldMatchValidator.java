package cz.lax.spring.structure.validation;

import org.apache.commons.beanutils.*;

import javax.validation.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 13.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {
	private String firstFieldName;
	private String secondFieldName;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(FieldMatch constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		try {
			Object firstObj = PropertyUtils.getProperty(value, firstFieldName);
			Object secondObj = PropertyUtils.getProperty(value, secondFieldName);

			if (firstObj == null && secondObj == null) {
				return true;
			}

			boolean matches = firstObj != null && firstObj.equals(secondObj);

			if (!matches) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate()).addPropertyNode(secondFieldName).addConstraintViolation();
			}

			return matches;
		} catch (Exception ignore) {
			// ignore
		}

		return true;
	}
}
