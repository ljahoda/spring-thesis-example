package cz.lax.spring.structure.common;

import cz.lax.spring.structure.config.*;
import org.springframework.beans.propertyeditors.*;
import org.springframework.web.bind.*;
import org.springframework.web.bind.annotation.*;

import java.text.*;
import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 11.03.14
 * Email: lukas@lukasjahoda.cz
 */
public abstract class AbstractGuiController extends AbstractController {

	/**
	 * Init binder.
	 *
	 * @param binder the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(AppConfig.DEFAULT_DATE_FORMAT);
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
