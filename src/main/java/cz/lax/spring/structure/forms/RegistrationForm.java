package cz.lax.spring.structure.forms;

import cz.lax.spring.structure.domain.*;
import cz.lax.spring.structure.validation.*;
import org.hibernate.validator.constraints.*;

import javax.validation.constraints.*;
import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 11.03.14
 * Email: lukas@lukasjahoda.cz
 */
@FieldMatch.List({
	@FieldMatch(first = "password", second = "confirmPassword", message = "{fieldMatch.password}"),
})
public class RegistrationForm {

	@NotBlank
	@Size(max = 50)
	String firstName;

	@NotBlank
	@Size(max = 50)
	String lastName;

	@NotNull
	Gender gender;

	@NotNull
	Date dateOfBirth;

	@NotBlank
	@Email
	@Size(max = 50)
	String email;

	@NotBlank
	@Size(max = 50)
	String password;

	@NotBlank
	@Size(max = 50)
	String confirmPassword;

	@NotNull
	Source source;

	@NotNull
	Boolean agreement;

	/**
	 * Gets first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets first name.
	 *
	 * @param firstName the first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets full name.
	 *
	 * @return the full name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets full name.
	 *
	 * @param lastName the full name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets gender.
	 *
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Sets gender.
	 *
	 * @param gender the gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * Gets date of birth.
	 *
	 * @return the date of birth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets date of birth.
	 *
	 * @param dateOfBirth the date of birth
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Gets email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets password.
	 *
	 * @param password the password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets confirm password.
	 *
	 * @return the confirm password
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Sets confirm password.
	 *
	 * @param confirmPassword the confirm password
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Gets source.
	 *
	 * @return the source
	 */
	public Source getSource() {
		return source;
	}

	/**
	 * Sets source.
	 *
	 * @param source the source
	 */
	public void setSource(Source source) {
		this.source = source;
	}

	/**
	 * Gets agreement.
	 *
	 * @return the agreement
	 */
	public Boolean getAgreement() {
		return agreement;
	}

	/**
	 * Sets agreement.
	 *
	 * @param agreement the agreement
	 */
	public void setAgreement(Boolean agreement) {
		this.agreement = agreement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Registration{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", gender=" + gender +
				", dateOfBirth=" + dateOfBirth +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", confirmPassword='" + confirmPassword + '\'' +
				", source=" + source +
				", agreement=" + agreement +
				'}';
	}

}
